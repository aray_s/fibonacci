package com.`as`.fibonaccinumbers

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.`as`.fibonaccinumbers.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var job: Job? = null

    private var previous = 0
    private var current = 1
    private var counter = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button.setOnClickListener {
            if (job?.isActive == true) cancel()
            else start()
        }

        savedInstanceState?.let {
            binding.editText.setText(it.getString("EDITTEXT"))
            binding.textView.text = it.getString("TEXTVIEW")
            previous=it.getInt("PREVIOUS")
            current=it.getInt("CURRENT")
            counter=it.getInt("COUNTER")+1
            if (it.getBoolean("ACTIVE")) start()
        }
    }

    private fun start() {
        binding.editText.isEnabled = false
        binding.button.text = getString(R.string.cancel)

        job = CoroutineScope(Dispatchers.Default).launch {
            while (counter<binding.editText.text.toString().toInt()) {
                val temp = previous
                previous=current
                current += temp
                Log.d("myTag", current.toString())
                withContext(Dispatchers.Main) {
                    binding.textView.text = getString(R.string.current_value, counter)
                    delay(1000)
                }
                counter++
            }

            withContext(Dispatchers.Main) {
                binding.editText.isEnabled = true
                previous=0
                counter=1
                binding.button.text = getString(R.string.start)
                binding.textView.text = getString(R.string.result, current)
                current=1
            }
        }
    }

    private fun cancel() {
        binding.editText.isEnabled = true
        binding.button.text = getString(R.string.start)
        previous=0
        current=1
        counter=1
        binding.textView.text = ""
        job?.cancel()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.run {
            putBoolean("ACTIVE", job?.isActive == true)
            putString("EDITTEXT", binding.editText.text.toString())
            putString("TEXTVIEW", binding.textView.text.toString())
            putInt("PREVIOUS", previous)
            putInt("CURRENT", current)
            putInt("COUNTER", counter)
        }
        cancel()
    }
}
